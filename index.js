console.log("Hello World from s15!");









/*
	Mini-Activity:

	Create the following variables and initialize their values:

	varialbe name: numString1, value: 5 as a string.
	varialbe name: numString2, value: 10 as a string.
	varialbe name: num1, value: 4 as an integer/number type.
	varialbe name: num2, value: 6 as an integer/number type.
	varialbe name: num3, value: 1.5 as an integer/number type.
	varialbe name: num4, value: .5 as an integer/number type.

	Optional: console log your variables to see if they are working.

*/
let numString1 = "5";
let numString2 = "10";
let num1 = 4;
let num2 = 6;
let num3 = 1.5;
let num4 = .5;
console.log(numString1, numString2, num1, num1, num3, num4);

// Lesson Proper

	// Operators - allows our programming languages to execute an operation or evaluation. In JS, when we use an operator a value is returned. For mathematical operators/operations, a number is returned. For comparison operators, a boolean is returned. There returned vlaues can be saved in a variable which we can later.

		// Mathematical Operators (+, -, /, *, %)

			console.log(num1+num2);//
			// with the use of + (addition operator), we can return a value from addition between two number types.

			let sum1 = num1+num2;
			console.log(sum1);//10

			let numString3 = numString1 + sum1;
			console.log(numString3);//510. It results to concatenation because at least one of the operators is a string. JS automatically converted the number to string because it detected that at least 1 of the operand is a string.


			/*
				number + number = proper mathematical addistion, results to sum.

				string + number = string. Coz instead, concatenation happens.
			*/

			let sampleStr = "Charles";
			console.log(sampleStr+num2);



			// Substraction Operators(-)
				// Allows us to subtract the operands and result to a diference.

				let difference1 = num1 + num3;
				console.log(difference1);

				/*
					For subtraction operator, the string is converted back to number and then subtracted.
				*/

				let difference2 = numString2 - num2;
				console.log(difference2);


				let sampleStr2 = "Joseph";
				let difference3 = sampleStr2 - num1;
				console.log(difference3); //NaN - not a number. "Joseph" was converted to a number and resulted to Nan, NaN-number will result to NaN.

				let difference4 = numString2 - numString1;
				console.log(difference4); //5 - number. Both numeric strings were converted into numbers and then subtracted.



				/*
					Mini-Activity
					
					Create a function called subtract () which will receie 2 numbers as an argument.


					Return the difference of both numbers.

					Save the returned value of the function into a variable called difference5.

					log the value of difference6 in the console.
				*/

				function subtract(num1,num2){
					return num1-num2;
				}


				let difference5 = subtract(25,5);
				console.log(difference5);




			// Multiplication Operator (*)
				// Multiply both operands and return the products as value.
				// Convert any string to number before multiplying.

				let product1 = num1*num2;
				console.log(product1);

				let product2 = numString1 * numString2;
				console.log(product2);

				/*
					Much like subtraction, string multiplied with a string, we convert the strings into numbers then multiply. It will result to proper multiplication.
				*/




			// Division Operator (/)

				//Divide right operand from the left operand.
					// leftOperand/rightOperand
					// Converts strings into numbers then divide.

				let quotient1 = num2 / num4;
				console.log(quotient1);

				let quotient2 = numString2/numString1;
				console.log(quotient2);



			/*MINI-ACTIVITY*/

			// MULTIPLY
			function multiply(num1, num2){
				return num1*num2;
			}
			let product3 = multiply(3,3);
			console.log(product3)



			// DIVIDE
			function divide(num1, num2){
				return num1/num2;
			}
			let quotient3 = divide(3,3);
			console.log(quotient3)





		// MODULO Operator (%)

			// Modulo is the remainder of a division operator.
			// 5/5 = 1 = 0
			console.log(5%5);//0
			// leftOperand%rightOperand = modulo or the remainder
			console.log(25%6);//1
			// 25/6 = remainder is 1
			console.log(50%2);//0
			// 50/2 = 25 = remainder is 0

			let modulo1 = 5%6;
			// We were able to save the value returned by the modulo operation which is 5.
			console.log(modulo1);


		// ASSIGNMENT Operators

			// Basic Assignment Operator (=)

				// Allows us to assing a value to the left operand.
				// Allows us to assing an initial value to variable.

				/*
					leftOperand = rightOperand
				*/

				let variable ="initial vlaue";
				variable = "new value";
				console.log(variable);

				let sample1 = 'sample value';
				variable = sample1;
				console.log(variable); //sample value
				console.log(sample1); //sample value


				let sample2 = "new sample value"
				variable = sample2;
				console.log(variable); //new sample value
				console.log(sample2);// the same as initial (new sample value.)

				/* a constant's value cannot be updated or re-assigned
				*/
				

				//Note: Do not add a basic assignment operator directly into the return statement

				function displayDetails(name,password,age){
					return {
						name: name,
						password: password,
						age: age,
					}
					}
					/* OR 

					let details = {
						name: name,
						password: password,
						age: age


						return details
					*/



		// ADDITION ASSIGNMENT OPERATOR (+=)
			// The result of the addition operation is re-assigned as the value of the left operand.

			let sum2 = 10;
			// 10 + 20 = 30
			// sum2 = 30 (result of addition)
			sum2 += 20;
			console.log(sum2);//30

			let sum3 = 5;
			// 5 + 5 = 10
			// sum3 = 10
			sum3 += 5;
			console.log(sum3);//10

			// When using addition assignment operator keep in mind that the left hand operand/left operand should be a variable.
			/* console.log(5+=); */

			let sum4 = 30;
			// 30 + "Curry = 30 will be converted into a string and will be concatenated with the string "Curry"
			// sum4 = "30Curry"
			sum4+="Curry";
			console.log(sum4);

			let sum5 = "50";
			// "50" + "50" = Since both are strings, it concatenated to "5050"
			// sum5 = "5050"
			sum5+="50";
			console.log(sum5);

			let fullName = "Wardell";
			let name1 = "Stephen";
			// "Wardell" + "Stephen" = "WardelStephen"
			// fullName = "WardellStephen"
			fullName+=name1;
			console.log(fullName);
			fullName+="Curry";
			// "WardellStephen" + "Curry" = "WardellStephenCurry"
			// fullName = "WardellStephenCurry"
			console.log(fullName);//"WardellStephenCurry"
			fullName+="II";
			console.log(fullName);




		// SUBTRACTION ASSIGNMENT OPERATOR (-=)

			// The result of subtraction between both operands will be saved/re-assigned as the value of the leftOperand

			let numSample = 50;
			// 50 - 10 = 40;
			// numSample = 40 (result of subtraction);
			numSample-=10;
			console.log(numSample);

			let numberString = "100";
			let numberString2 = "50";
			// "100" - "50" = both strings are converted to numbers and subtracted = 100 - 50 = 50
			// numberString = 50
			numberString-=numberString2;
			console.log(numberString);//50

			let text = "ChickenDinner";
			// "ChickenDinner" - "Dinner". Both converted to numbers but since they are alphanumeric/words, it resulted to NaN(not a number).
			// NaN - NaN = NaN
			// text = NaN
			text -= "Dinner";
			console.log(text);//NaN


					
				

		// MULTIPLICATION ASSIGNMENT OPERATOR (*=)


			// The result of multiplication between the operands will be re-assigned as the value of the left operand

			let sampleNum1 = 3;
			let sampleNum2 = 4;
			// 3*4 = 12;
			// sampleNum1 = 12
			sampleNum1*=sampleNum2;
			console.log(sampleNum1);//12
			console.log(sampleNum2);//4

			let sampleNum3 = 5;
			let sampleNum4 = "6";
			// 5*"6" = conversion to numbers = 5*6 = 30
			// sampleNum3 = 30
			sampleNum3*=sampleNum4;
			console.log(sampleNum3);





		// DIVISION ASSIGNMENT OPERATOR (/=)

			// The result of division between both operands will be reassigned as the value of the leftOperand.

			let sampleNum5 = 70;
			let sampleNum6 = 10;
			//70/10 = 7
			// sampleNum5 = 7
			sampleNum5/=sampleNum6;
			console.log(sampleNum5);//7

			let sampleNum7 = 45;
			sampleNum7/=0;
			console.log(sampleNum7);
			// Infinity. This is invalid division because we should not divide by 0.





		// Order of operations follow (MDAS)

			let mdasResult = 1 + 2 - 3 * 4 / 5;
			console.log(mdasResult); //0.6

			/*
				M: 3*4 = 12
				D: 12/5 = 2.4
				A: 1+2 = 3
				S: 3-2.4 = 0.6
			*/

			// PEMDAS = Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction

			let pemdasResult = 1 + (2-3)*(4/5);
			console.log(pemdasResult); //0.2
			/*
				P: 4/5 = .8
					   = 2-3 = -1

				M: -1*8 = -.8
				D:
				A: 1+ -.8 = .2
				S:
			*/




		// INCREMENT AND DECREMENT

			// Incrementation and Decrementation is adding or subtracting from a variable and then re-assigning the result to the variable.

			// There are 2 implementations of this: Pre-fix and Post-fix

			let z= 1;

			// Pre-Fix:
			++z;
			console.log(z); //2 - The value of z was added with 1 and it is immediately returned. With pre-fix increment/decrement, the incremented/decremented value is returned at once.

			// Post-Fix
			console.log(z++);//2
			console.log(z);//3
			// With post-fix incrementation, we add 1 to the  value of the variable, however, the difference is that the previous value is returned first before the incremented value.

			z++;//incrementation
			console.log(z);//incremented value to 4

			// Prefix vs Postfix
			console.log(++z);//5 increment first and then returned the incremented value.
			console.log(z++);//5 previous value is returned first vefore incrementation.


			//Prefix and Postfix Decrementation

			console.log(--z);//5 - with prefix decrementation the result of subtraction by 1 is returned immediately.
			console.log(z--);//5 - with postfix decrementation the result of subtraction by 1 is not immediately returned, instead the previous will be returned.
			console.log(z);//4 - the new value is returned after postfix decrementation.

			// Can we increment or decrement raw number data?

				// Incrementation/decrementation are used on variables that contain number types.




			// Can we increment or decrement a string?

				let sampleNumString = "5";
				console.log(++sampleNumString);//6 - a numberic string is converted to number and will be incremented

				let sampleString = "James";
				console.log(sampleString++);//NaN - incrementation/decrementation will convert your string first into a number, however, since the string contains alphanumeric characters, it converted to NaN.




	// COMPARISON OPERATORS

		// Comparison Operators are used to compare  the values of the left and right operands.
		// Comparison Operators return a boolean (true/false)





			// Loose Equality Operator

				console.log(1 == 1);

				// We can also save the result of comparison in a variable.

				let isSame = 55 == 55;
				console.log(isSame);

				console.log(1 == '1');//true - loose quality operator prioritizes the sameness of the value because Loose Equality Operator actually enforces what we call Forced Coercion or when the types of the operands are automatically changed before the comparison. The string here was actually converted to number. 1 == 1 = true.

				console.log(0 == false);//true - with forced coercion, since the operands have different types, both were converted number, 0 is already a number but false is a boolean, the boolean false when converted to a number is equal to 0. And thus, 0 == 0 = true.

				// JS has a function/method which will allow us to convert data from one type to number

				let sampleConvert = Number(false);
				console.log(sampleConvert);//0 with the use of the Number() method we can change the type of data to number type.

				let sampleConvert2 = "2500";
				sampleConvert2 = Number(sampleConvert2);
				console.log(sampleConvert2);

				console.log(1 == true);//true - the boolean true when converted to number is 1. So, 1 == 1 = true.

				console.log(5 == "5");//true - same value; different type, with loose equality, JS does fored coercion before evaluation.

				console.log(true == "true");//false - forced coercion wherein both operands are converted to numbers. Boolean true was converted to a number as 1. String "true" was converted to a number but since it was a word/alphanumeric, it resulted to NaN 
				//1 == NaN is falce.

				console.log(false == "false");//false - fored coercion: boolean false converted to 0, string "false" converted to NaN 
				//0 == NaN is false.






			// Strict Equality Operator

				// Strict Equality Operator evaluates the sameness of both vlaues and types of operands. Thus, strict equality operator is more preferred because JS is a loose-typed language.

				console.log(1 == "1");//In loose equality (==) this is true.
				console.log(1 === "1");//false - checks sameness of both values and types of the operands.

				console.log("James2000" === "James2000");//true - it is case sensitive. if you change capitalization of letter then it will become false.

				console.log(55 === "55");//false - strict equality operator (===) checks sameness of value AND type.





			// Loose Inequality Operator

				// Checks whether the operands are NOT equal and or/have different values.
				// Much like loose Equality Operator, loos inequality operator alos foes forved coercion.

				console.log(1 != "1");//false
				// Forced Coercion:
				// both operands converted to number:
				// string "1" is also converted to number.
				// 1 != 1 = equal so: not inequal = false.

				/*
				Loose Inequality Operator returns true if the operands are NOT equal, it will false if the operands are found to be equal.
				*/

				console.log("James" != "John");//true - James is not equal to John.

				console.log(5 != 55);//true - 5 is NOT(!=) equal to 55.
				console.log(1500 != "5000");//true
				// Forced Coercion:
				// 1500 will be converted to number
				// "5000" will be converted to number
				// 1500 != 5000 is NOT(!=) equal

				console.log(true != "true");//true
				// with forced coercion: true was converted to 1
				// "true" was converted to number but resulted to NaN
				// 1 is NOT equal to NaN
				// It is inequal(!=)





			// Strict Inequality Operator

				// Strict Inequality Operator will check whether the two operands have inequal type or value.

				console.log(5 !== 5);
				// false - operands have equal value and same type.
				console.log(5 !== "5");
				// truw - operands have same value they have different types.
				console.log(true != "true");
				// true - operands are inequal coz they have different types.



			// Equality Operators and Inequality Operators with Variables:

				let nameStr1 = "Juan";
				let nameStr2 = "Jack";
				let numberSample = 50;
				let numberSample2 = 60;
				let numStr1 = "15";
				let numStr2 = "25";

				console.log(numStr1 == 50);//false
				console.log(numStr1 == 15);//true
				console.log(numStr2 === 25);//false
				console.log(nameStr1 != "James");//true
				console.log(numberSample !== "50");//true
				console.log(numberSample != "50");//false
				console.log(nameStr1 == nameStr2);//false
				console.log(nameStr2 === "jack");//false





	// RELATIONAL COMPARISON OPERATORS

		// A comparison operator which will check the relationship between operands and return boolean

		let price1 = 500;
		let price2 = 700;
		let price3 = 8000;
		let numStrSample = "5500"

		// greater than (>)
		console.log(price1 > price2);//false
		console.log(price3 > price2);//true
		console.log(price3 > numStrSample);//true - forced coercion to change the string to a number.


		// less than
		console.log(price2 < price3);//true
		console.log(price1 < price3);//true






		/*
		if(user1.level >= 35){
			conconsole.log("Hello, Knight!");
		} else if (user1.level >= 25){
			console.log("Hello, Swordsman!");
		} else if(user1.level >= 10) {
			console.log("Hello, Rookie!");
		} else {
			console.log("Level out of range");
		}	
		*/





		// Logical Operators for If conditions:

		let usernameInput1 = "NicoleIsAwesome100";
		let passwordInput1 = "iamawesomenicole";

		let usernameInput2 = "";
		let passwordInput2 = null;




		function register(username,password){

			if(username === "" || password === null){
				console.log("Please complete the form");
			} else {
				console.log("Thank you for registering");
			}
		}

		register(usernameInput1,passwordInput1);
		register(usernameInput2,passwordInput1);




		function requirementChecker(level,isGuildAdmin){

			if(level <= 25 && isGuildAdmin === false){
				console.log("Welcome to the Newbies Guild");
			} else if (level >25) {
				console.log("You are too strong");
			} else if (isGuildAdmin === true){
				console.log("You are a guild admin.");
			} 
		}


		
		/*
		let user3 = {
			username: "richieBillions"
			age: 20,
			level: 20,
			isGuildAdmin: true
		}
		requirementChecker(user3.level,user3.isGuildAdmin);//user3 cannot join
		*/



		function addNum(num1,num2){

			// Check if the numbers being passed are both number types.
			// typeof keyword returns a string which tells the type of data that follows it
			if (typeof num1 === "number" && typeof num2 === "number"){
				console.log("Run only if both arguments passed are number type");
			} else {
				console.log("One or both of the arguments are not numbers.");
			}
		}

		addNum(5,10);//run the if statement because both arguments are numbers.
		addNum("6",20);//run the else because one of the arguments is not a number.

		//typeof - used for validating the data type of variables or data.
		// It returns a string after evaluating the data type of the data/variable that comes after it.
		
		let str = "sample"
		console.log(typeof str);

		console.log(typeof 75);



		/* Mini-Activity 
	
		
		Create a function called dayChecker() which is able to reeive a single string as an argument.

		If the value of the argument given is Sunday, show a message in the console: "Today is Sunday; Wear White."

		Else if the value of the argiment given is Monday, show a message in the console: "Today is Monday; Wear Blue."

		Else if the value of the argument given is Tuesday, show a message in the console: "Today is Tuesday; Wear Green"

		Else if  the value of the argument given is Wednesday, show a message in the console: "Today is Wednesday; Wear Purple."

		Else if  the value of the argument given is Thursday, show a message in the console: "Today is Thursday; Wear Brown."

		Else if  the value of the argument given is Friday, show a message in the console: "Today is Friday; Wear Red."

		Else if  the value of the argument given is Saturday, show a message in the console: "Today is Saturday; Wear Pink."

		*/



		/*
			function dayChecker(day){


			// toLowercase() method makes a string all small caps
			if(day === Sunday){
				console.log("Today is Sunday; Wear White.");
			} else if (day === Monday) {
				console.log("Today is Monday; Wear Blue.");
			} else if (day === Tuesday){
				console.log("Today is Tuesday; Wear Green.");
			} else if (day == Wednesday){
				console.log("Today is Wednesday; Wear Purple.");
			} else if (day === Thursday){
				console.log("Today is Thursday; Wear Brown.");
			} else if (day === Friday){
				console.log("Today is Friday; Wear Red.");
			} else if (day === Saturday){
				console.log("Today is Saturday; Wear Pink.");
			} 
		}
		*/

		/* 
			Switch is a conditional statement which can be an alternative to else-if,
			if-else, where the data being checked or evaluated is of an expected input.

			Switch will compare your expression/condition to match with a case. Then the statement for that case will run.

			syntax:

				switch(expression/condition){
					case: value:
						statement;
						break;
					default:
						statement;
						break;
				}
		*/



		/*
			= basic assignment operator
			== Loose Equality
			=== Strict Equality
		*/

		

		// Eugene, Vincent, Dennis, Alfred, Jeremiah

		// A switch which display the power level of a selected member

		let member = "Jeremiah";

		// the BREAK statement is used to terminate the execution of the switch after it found a match

		switch(member){
			case "Eugene":
				console.log("You're power level is 20000");
				break;
			case "Dennis":
				console.log("Your power level is 15000");
				break;
			case "Vincent":
				console.log("Your power level is 14500");
				break;
			case "Jeremiah":
				console.log("Your power level is 10000");
				break;
			case "Alfred":
				console.log("Your power level is 8000");
				break;
			default:
				console.log("Invalid Input. Add a Ghost Fighter member.");
		}

		/* 
			Mini-Activity
		
				Create a new function with the name capitalChecker() which is able  to receive a single string as an argument.

				Then add switch which will evaluate and display a message on the console if the argument matches a case.

				Also add a default statement

				Here are the cases:

				If the condition matches "philippines"
				display "Manila" in the console.

				If the condition matches "usa"
				display "Washington D.C. in the console.

				If the condition matches "japan"
				display "Tokyo" in the console.

				If the condition matches "germany"
				display "Berlin" in the console.

				If the case doesnt match the condition, then show the following message:
					"Input is out of range. Choose another country."

		*/

		function capitalChecker(country){

		switch(country){

			case "Philippines":
				console.log("Manila");
				break;
			case "USA":
				console.log("Washington D.C.");
				break;
			case "Japan":
				console.log("Tokyo");
				break;
			case "Germany":
				console.log("Berlin");
				break;
			default:
				console.log("Input is out of range. Choose another country.");
			}
		}

		capitalChecker("USA")

		// Ternary Operator

			// Conditional statement as if-else. However it was introduced to be short hand way to write if-else.


		let superHero = "Batman"

		superHero === "Batman" ? console.log("You are rich.") : console.log("Bruce Wayne is richer than you.");

		// Ternary operators require an else statement

		/* superHero === "Superman" ? console.log("Hi, Clark!"); */

		// Ternary Operators can implicitly return value even without the return keyword.

		let currentRobin = "Dick Grayson";

		let isFirstRobin =  currentRobin === "Dick Grayson" ? true : false
		console.log(isFirstRobin);





		