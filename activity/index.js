let user1 = {
			username: "jackscepticeye",
			email: "seanScepticeye@gmail.com",
			age: 31,
			topics: ["Gaming", "Commentary"]
		}



/*
	Activity:

	Create a new function called isLegalAge(). This function is able to receive an object as an argument.

*/

function isLegalAge(age){

	if(age > 18) {
		console.log(age, "Welcome to the Club")
				}

	else{
		console.log("You cannot enter the club yet.")
		}
}



function addTopic(topic){
	if(topic.length >= 5){
		console.log(user1.topics.push(topic))
	} else {
		console.log("Enter a topic with at least 5 characters.")
	}
}

let clubMembers = ["jackscepticeye", "pewdiepie"]

function register(user){
	if(user.length >= 8){
		console.log(clubMembers.push(user));
	} else {
		console.log("Please enter a username longer or equal to 8 characters.")
	}
}


isLegalAge(user1);
addTopic("Math");
addTopic("Comedy");
console.log(user1.topics)
register("markiplier");
console.log(clubMembers);
register("AaronKeane")
console.log(clubMembers);